
const getRandom = (max = 1, min = 0) => {
    if (max <= 0) return 0; 
    return Math.floor(Math.random() * (max - min)) + min;
}

const getRandomWithExclude = (max = 1, min = 0, exclude = []) => {
    const arr = [];
    for (let i = min; arr.length < max - min && i < max; i += 1) {
        const isEx = exclude.find((val) => i === val); 
        if (isEx === undefined || isEx === null){
            arr.push(i);
        }
    }
    return arr[getRandom(arr.length)];
}

class GameController {
    
    constructor(props) {
        const {container, bacteriumsCount} = props;
        this.bacteriums = [];
        let modLock = false;
        for (let i = 0; i < bacteriumsCount; i += 1) {
            const bac = new Bacterium(i + 1);
            this.bacteriums.push(bac);
        }

        this.getRandomConnection(this.bacteriums)

        const gameObj = {
            nodes: this.bacteriums.map((bac) => bac.getNode()),
            edges: this.bacteriums.reduce((acc, bac) => acc.concat(bac.getEdges()), [])
        }
        const network = new vis.Network(container, gameObj, {});
        this.bacteriums.map((bac) => bac.network = network);
        this.ctx = document.getElementById('ctx');
        network.on('doubleClick', (event) => {
            const currBacterium = this.bacteriums[event.nodes[0] - 1];
            if (!modLock && currBacterium) {
                const coords = event.pointer.DOM;
                this.ctx.style.display = 'flex';
                coords.y = coords.y + this.ctx.offsetHeight > container.clientHeight ?
                    container.clientHeight - this.ctx.offsetHeight :
                    coords.y;
                coords.x = coords.x + this.ctx.offsetWidth > container.clientWidth ?
                    container.clientWidth - this.ctx.offsetWidth :
                    coords.x;
                ctx.style.top = `${coords.y}px`;
                ctx.style.left = `${coords.x}px`;
                Array.from(ctx.querySelectorAll('div')).map((div) => {
                    div.onclick = () => {
                        this.hideCtx();
                        const type = div.innerText.toLowerCase();
                        currBacterium.type = type;
                    }
                });
            }
        });
        network.on('click', () => this.hideCtx());
        network.on('resize', () => this.hideCtx());
        this.currIter = 0;
    }

    nextStep() {
        const nullBacteriums = this.bacteriums.filter((bac) => bac.type === null);
        if (nullBacteriums.length === 0) {
            this.currIter += 1;
            this.bacteriums.filter((bac) => bac.type === 'virus')
            .map((bac) => bac.iteration += 1)
        } else {
            alert('You must specify type for all nodes');
        }
    }

    hideCtx() {
        this.ctx.style.display = 'none';
    }

    getRandomConnection(bacteriums) {

        const resolvedConnections = [];

        bacteriums.forEach((bacterium, ind) => {
            const currConnections = resolvedConnections
            .filter((conn) => conn.to === ind);
            const maxConnections = bacteriums.length - currConnections.length - 1;
            const connectionsLim = getRandom(maxConnections, 1);
            const getCurrentEx = () => resolvedConnections
                .filter((conn) => (conn.to === ind || conn.from === ind))
                .reduce((acc, conn) => {
                    if (conn.from === ind) {
                        return acc.concat(conn.to);
                    } else {
                        return acc.concat(conn.from);
                    }
                }, []);
            for (let i = 0; i < connectionsLim; i += 1) {
                const to = getRandomWithExclude(bacteriums.length, 0, getCurrentEx().concat([ind]));
                resolvedConnections.push({from: ind, to});
                bacterium.connect(bacteriums[to]);
            }
        });

    }

}