
class Bacterium {

    constructor(id) {
        const _id = id || new Date();
        this.network = null;
        const _typeColors = {
            virus: '#f44336',
            good: '#4caf50',
            immunity: '#2196f3'
        }
        let _type = null;
        this.nodeCfg = {
            color: '#ff9800',
            shape: 'dot',
            size: 30,
            font: '16px arial white',
            label: _id.toString(),
            physics: false
        }
        this.edgeCfg = {
            width: 5,
            color: '#fafafa'
        }
        this.connectedBacteriums = [];
        let _iteration = 0;

        const _defineProperty = (name, prop) => {
            Object.defineProperty(this, name, prop);
        }

        _defineProperty('id', {
            get: () => _id
        });
        _defineProperty('iteration', {
            get: () => _iteration,
            set: (val) => {
                while (_iteration < val) {
                    _iteration += 1;
                    this.onIteration();
                }
            } 
        });
        _defineProperty('type', {
            get: () => _type,
            set: (val) => {
                const color = _typeColors[val];
                if (color && this.network) {
                    this.network.clustering.updateClusteredNode(this.id, {color});
                    _type = val;
                    _iteration = 0;
                } else {
                    throw Error('BAD BACTERIUM TYPE OR NETWORK DEFINITION ERROR');
                }
            }
        });
    }

    onIteration() {
        if (this.network) {
            if (this.type === 'virus') {
                this.connectedBacteriums.filter((conn) => conn.bacterium.type === 'good')
                .map((conn) => conn.bacterium.type = 'virus');
                if (this.iteration === 2) {
                    this.type = 'immunity';
                }
            }
        } else {
            throw Error('NO NETWORK HAS BEEN CONNECTED');
        }
    }

    getNode() {
        return {
            id: this.id,
            ...this.nodeCfg
        }
    }

    getEdges() {
        return this.connectedBacteriums
        .filter((obj) => !obj.collision)
        .map((obj) => {
            return {
                from: this.id,
                to: obj.bacterium.id,
                ...this.edgeCfg
            }
        });
    }
    
    connect(bacterium) {
        if (bacterium) {
            this.connectedBacteriums.push({bacterium, collision: false});
            bacterium.connectedBacteriums.push({bacterium: this, collision: true});
        } else {
            throw Error('BACTERIUM WAS NOT PASSED');
        } 
    }

}